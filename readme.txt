=== WPK Project Configurator ===
Contributors: rossjames96
Requires at least: 2.5.0
Tested up to: 4.5
Stable tag: 1.0.1
License: GPLv3

Configuration plugin for syncing themes and plugins between wordpress installs.

== Description ==
Configuration plugin for syncing themes and plugins between wordpress installs.

== Installation ==
1. Extract the .zip file for this plugin and upload its contents to the `/wp-content/plugins/` directory.
2. Activate the plugin through the "Plugins" menu in WordPress.